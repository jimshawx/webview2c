# WebView2C
An example of how to use WebView2 from C.

Pretty much everything you need to run a WebView2 control in your Windows app.

You will need to install the WebView2 runtime from https://developer.microsoft.com/en-us/microsoft-edge/webview2/  
You also need a recent version of Edge installed, but there's no need to be on the Developer or Canary stream for this to work.

License: Public Domain

