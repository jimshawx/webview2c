#include <windows.h>
#include <shlobj.h>

#define interface struct
#include <WebView2.h>

// Global Variables:
HINSTANCE hInst;                                // current instance
HWND hWnd;                                      // current window
WCHAR *szTitle = L"WebView2";                  // The title bar text
WCHAR *szWindowClass = L"WebView2";            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    HRESULT err = CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
    if (err != S_OK)
        return 0;

    // Initialize global strings
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    MSG msg;
    // Main message loop:
    while (GetMessage(&msg, NULL, 0, 0))
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    CoUninitialize();

    return (int)msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = NULL;
    wcex.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = NULL;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = NULL;

    return RegisterClassExW(&wcex);
}

#include <assert.h>
#include <stdio.h>
#define MAX_LOG_MSG 4000
void elog(const wchar_t *fmt, ...)
{
    wchar_t tmp[MAX_LOG_MSG];
    va_list v;
    va_start(v, fmt);
    vswprintf_s(tmp, MAX_LOG_MSG, fmt, v);
    va_end(v);
    OutputDebugString(tmp);
}

void ShowError(DWORD err)
{
    LPWSTR address;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&address, 0, NULL);
    elog(address);
    LocalFree(address);
    err = GetLastError();
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, err, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&address, 0, NULL);
    elog(address);
    LocalFree(address);
}

ICoreWebView2Environment *m_uiEnv;
ICoreWebView2Controller *m_uiControl;
ICoreWebView2 *m_controlsWebView;

EventRegistrationToken m_controlsUIMessageBrokerToken = {0};

#define ESC(...) __VA_ARGS__
#define IUNKNOWN_WITH_INVOKE(name, type, invoke_signature, invoke_impl)\
static int name ## _reference_counter = 1;\
static HRESULT __stdcall name ## _QueryInterface(type *This, REFIID riid, void **ppvObject) { This; riid; ppvObject = NULL; return S_OK; }\
static ULONG __stdcall name ## _AddRef(type *This) { This; return ++ name ## _reference_counter; }\
static ULONG __stdcall name ## _Release(type *This) { This; return -- name ## _reference_counter; }\
static HRESULT __stdcall name ## _Invoke ( type *This, invoke_signature ) invoke_impl ;\
static type ## Vtbl name ## _vtbl = {name ## _QueryInterface, name ## _AddRef, name ## _Release, name ## _Invoke};\
type name = { & name ## _vtbl }

IUNKNOWN_WITH_INVOKE(navigation_complete_handler, ICoreWebView2NavigationCompletedEventHandler, ESC(ICoreWebView2 *sender, ICoreWebView2NavigationCompletedEventArgs *args),
{
    OutputDebugString(L"navigation complete!\n");
    return S_OK;
});

IUNKNOWN_WITH_INVOKE(web_message_received_handler, ICoreWebView2WebMessageReceivedEventHandler, ESC(ICoreWebView2 *sender, ICoreWebView2WebMessageReceivedEventArgs *args),
{
    OutputDebugString(L"web message!\n");
    return S_OK;
});

IUNKNOWN_WITH_INVOKE(create_webview2_controller_handler, ICoreWebView2CreateCoreWebView2ControllerCompletedHandler, ESC(HRESULT result, ICoreWebView2Controller *created_controller),
{
    This; created_controller;

    if (result != S_OK || created_controller == NULL)
    {
        ShowError(result);
        return result;
    }

    created_controller->lpVtbl->AddRef(created_controller);

    m_uiControl = created_controller;

    HRESULT err;
    err = m_uiControl->lpVtbl->get_CoreWebView2(m_uiControl , &m_controlsWebView);
    assert(err == S_OK);

    m_controlsWebView->lpVtbl->AddRef(m_controlsWebView);

    ICoreWebView2Settings *settings;
    err = m_controlsWebView->lpVtbl->get_Settings(m_controlsWebView, &settings);
    //err = settings->lpVtbl->put_AreDevToolsEnabled(settings, FALSE);

    err = m_controlsWebView->lpVtbl->add_WebMessageReceived(m_controlsWebView , &web_message_received_handler, &m_controlsUIMessageBrokerToken);
    assert(err == S_OK);

    err = m_controlsWebView->lpVtbl->add_NavigationCompleted(m_controlsWebView, &navigation_complete_handler, &m_controlsUIMessageBrokerToken);
    assert(err == S_OK);

    RECT rect;
    GetClientRect(hWnd, &rect);
    err = m_uiControl->lpVtbl->put_Bounds(m_uiControl, rect);
    assert(err == S_OK);

    UpdateWindow(hWnd);

    // Workaround for black controls WebView issue in Windows 7
    HWND wvWindow = GetWindow(hWnd, GW_CHILD);
    while (wvWindow != NULL)
    {
        UpdateWindow(wvWindow);
        wvWindow = GetWindow(wvWindow, GW_HWNDNEXT);
    }

    err = m_controlsWebView->lpVtbl->Navigate(m_controlsWebView, L"https://news.ycombinator.com");
    assert(err == S_OK);

    return S_OK;
});

IUNKNOWN_WITH_INVOKE(create_webview2_environment_handler, ICoreWebView2CreateCoreWebView2EnvironmentCompletedHandler, ESC(HRESULT result, ICoreWebView2Environment *created_environment),
{
    This;

    if (result != S_OK || created_environment == NULL)
    {
        ShowError(result);
        return result;
    }

    created_environment->lpVtbl->AddRef(created_environment);

    m_uiEnv = created_environment;

    HRESULT err;
    err = m_uiEnv->lpVtbl->CreateCoreWebView2Controller(m_uiEnv, hWnd, &create_webview2_controller_handler);
    assert(err == S_OK);

    return S_OK;
});

wchar_t *GetAppDataDirectory()
{
    static wchar_t path[MAX_PATH];

    HRESULT err = SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path);
    if (err != S_OK)
    {
        wcscpy_s(path, MAX_PATH, L".\\");
        return path;
    }

    wcscat_s(path, MAX_PATH, L"\\Microsoft\\");
    wcscat_s(path, MAX_PATH, szTitle);
    return path;
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
      return FALSE;

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   wchar_t *user_data = GetAppDataDirectory();
   wcscat_s(user_data, MAX_PATH, L"\\User Data");

   HRESULT err = CreateCoreWebView2EnvironmentWithOptions(NULL, user_data, NULL, &create_webview2_environment_handler);
   if (err != S_OK)
       ShowError(err);


   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    case WM_GETMINMAXINFO:
    {
        MINMAXINFO *minmax = (MINMAXINFO *)lParam;
        minmax->ptMinTrackSize.x = 900;
        minmax->ptMinTrackSize.y = 400;
    }
    break;
    case WM_SIZE:
    case WM_DPICHANGED:
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

